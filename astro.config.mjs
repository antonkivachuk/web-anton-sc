import image from "@astrojs/image";
import tailwind from "@astrojs/tailwind";
import { defineConfig } from "astro/config";
import netlify from '@astrojs/netlify/functions';

import react from "@astrojs/react";

// https://astro.build/config
export default defineConfig({
  site: "https://sacacitas.gitlab.io",
  // base: '/web/',
  integrations: [tailwind(), image({
    serviceEntryPoint: "@astrojs/image/sharp"
  }), react()],
  vite: {
    ssr: {
      external: ["svgo"]
    }
  },
  outDir: 'public',
  publicDir: 'static',
  adapter: netlify(),
  output: 'server',
});